//
//  Constants.swift
//  Slack
//
//  Created by Priitsh Sawant on 03/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import Foundation

let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND = "unwindToChannel"
